package Repository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import Domain.MonitoredData;

public class Repository 
{
	public ArrayList<MonitoredData> data;
	
	public Repository() 
	{
		data = new ArrayList<>();
		getData();
	}
	
	public void getData() 
	{
		File file = new File("src/activities.txt"); // buffer
		BufferedReader reader;
		try 
		{
			reader = new BufferedReader(new FileReader(file));
			// se va salva in line textul de pe o linie din fisier
			String line;
			// se va salva fiecare camp din randul din fisier
			//( camp = text despartit de restul liniei printr-un numar de taburi)
			String[] fields;
			// citeste linie cu linie pana nu mai sunt linii ( citeste null)
			while ((line = reader.readLine()) != null) 
			{
				fields = line.trim().split("\\t+"); // sterge spatiile din lateralele liniei si apoi inparte dupa tab
				// adauga obiectul citit in lista de date
				data.add(new MonitoredData(fields[0],fields[1],fields[2]));
			}
		} 
		catch (FileNotFoundException e) 
		{
			System.out.println("File not found");
		} 
		catch( IOException e) 
		{
			System.out.println("File not found");
		}
	 }
	
	public static void main(String [ ] args) 
	{
		new Repository();
	}
}
