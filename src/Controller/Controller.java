package Controller;

import java.util.List;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Map;
import java.util.stream.Collectors;

import Repository.Repository;

public class Controller
{
	public int statistic0;
	public Map<String, Long> statistic1;
	public Map<Integer, Map<String, Long>> statistic2;
	public List<String> statistic5;
	private Repository r;
	
	public Controller(Repository r) 
	{
		this.r = r;
	}
	
	// cerinta 1
	public void genStatistic1() 
	{
		statistic0 = (int) r.data.stream().map( i->{ return i.getStartTime().split("-")[2].split("\\s+")[0]; })    //map cu key-urile de la start time
				                                .distinct()
                                                .count();	
	}
	
	// cerinta 2
	public void genStatistics2() 
	{
		statistic1 = r.data.stream().
			            collect (
					            	Collectors.groupingBy(i->{ return i.getLabel(); }, // key
					            	Collectors.counting()) /// value
			                    );
	}
	
	// cerinta 3
	public void genStatistics3() 
	{
		statistic2 = r.data.stream().
						collect ( 
									Collectors.groupingBy(i->{  String[] token = i.getStartTime().split("-")[2].split("\\s+");
																return Integer.parseInt(token[0]); },
									Collectors.groupingBy(i->{  return i.getLabel(); },
									Collectors.counting()))
								);
	}
	
	// cerinta 5
	public void genStatistic5() 
	{
		genStatistics2();
																	//string to date
        Map<Object, Long> finalMap = r.data.stream().filter(i->{ DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
													            long time = 0;
													            try 
													            {
													                time = format.parse(i.getEndTime()).getTime() - format.parse(i.getStartTime()).getTime();
													            } 
													            catch (ParseException e) 
													            {
													                e.printStackTrace();
													            }
													            if (time < 5 * 60 * 1000) 
													            {
													                return true;
													            } 
													            else 
													            {
													                return false;
													            }
													            // creem un map cu activitatea si durata sa
        													})
        													.collect(
        																Collectors.groupingBy(i->i.getLabel(),
        																Collectors.counting())
        															);
        
        statistic5 = r.data.stream().filter(i->{ if(finalMap.get(i.getLabel()) == null) 
									        	 {
									                return false;   //	have 90% of the monitoring samples with duration less than 5 minutes
									             } 
        										 else if (finalMap.get(i.getLabel()) >= 0.9 * statistic1.get(i.getLabel())) 
									             {
									                return true;
									             } 
									             else 
									             {
									                return false																			;
									             }
        								  })
        							.map(i->i.getLabel())																			
        							.distinct()
        							.collect(Collectors.toList());
        this.writeToFile(statistic5);
	}
	
	private void writeToFile(List<String> l) 
	{
		PrintWriter writer;
		try 
		{
			writer = new PrintWriter("src/output.txt", "UTF-8");
			writer.println(l.toString());
			writer.close();		
		} 
		catch (FileNotFoundException | UnsupportedEncodingException e) 
		{
			e.printStackTrace();
		}
	} 
	
	public static void main(String[] args) 
	{
		Controller c = new Controller(new Repository());
		c.genStatistic1();
		c.genStatistics2();
		c.genStatistics3();
		c.genStatistic5();
		System.out.println("Task 1 -> " + c.statistic0);
		System.out.println("Task 2 ->" + c.statistic1.toString());
		System.out.println("Task 3 ->" + c.statistic2.toString());
        System.out.println("Task 5 ->" + c.statistic5.toString());
	}
	
}
