package Domain;

public class MonitoredData
{
		private String startTime;
		private String endTime;
		private String label;
		
		public MonitoredData(String startTime, String endTime, String label)
		{
			this.setStartTime(startTime);
			this.setEndTime(endTime);
			this.setLabel(label);
		}	
		
		public String toString() 
		{
			return "Start Time: " + getStartTime().toString() + " End Time: " + getEndTime().toString() + " Label: " + getLabel() + "\n";
		}
		
		public static void main(String [ ] args) 
		{
			new MonitoredData("2011-11-28 02:27:59","2011-11-28 10:18:11","Sleeping");
		}

		public String getEndTime() 
		{
			return endTime;
		}

		public void setEndTime(String endTime) 
		{
			this.endTime = endTime;
		}

		public String getStartTime() 
		{
			return startTime;
		}

		public void setStartTime(String startTime) 
		{
			this.startTime = startTime;
		}

		public String getLabel()
		{
			return label;
		}

		public void setLabel(String label) 
		{
			this.label = label;
		}
}
